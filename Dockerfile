FROM python:3.7

WORKDIR /app

COPY requirements.txt .

RUN pip3 install -r requirements.txt

COPY . .

ENV FLASK_APP=app

EXPOSE 5000

CMD ["flask", "run", "--host", "0.0.0.0"]
