# Flask PyTest Example

Simple example application showing off how to use PyTest with Flask for testing routes.

## Instructions
1. Make a new virtual environment and activate it
```bash
$ virtualenv venv && source ./venv/bin/activate
```

2. Install the requirements
```bash
$ make init
(OR)
$ pip install -r requirements.txt
```

3. Run the tests
```bash
$ make test
(OR)
$ pytest -v
```
