init:
	pip install -r requirements.txt

test:
	pytest -v

run:
	flask run --host 0.0.0.0
